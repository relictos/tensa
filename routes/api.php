<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('currencies', 'ApiController@currencies');
Route::get('categories', 'ApiController@categories');

Route::get('goods', 'ApiController@goods');
Route::get('goods/{slug}', 'ApiController@goodInfo');

Route::post('orders', 'ApiController@makeOrder');
