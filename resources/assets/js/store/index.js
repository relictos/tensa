import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store(
    {
        state: {
            currencies: [],
            categories: [],
            goods: [],
            highlighted: [],
            total_count: 0,
            page_count: 10,
            current_page: 0,
            max_page: 0,
            current_good: null
        },
        getters: {
            goods: function(state) {
                return state.goods
            },
            goodsCount: function(state) {
                return state.goods.length
            },
            goodsPage: function(state) {
                return state.current_page
            },
            goodsExceed: function(state) {
                return state.current_page == state.max_page;
            },
        },
        mutations: {
            SET_GOODS (state, goods) {
                state.goods = goods
            },
            ADD_GOOD (state, good) {
                state.goods.push(good)
            },
            ADD_HIGHLIGHTED (state, good) {
                state.highlighted.push(good)
            },
            REMOVE_GOOD (state, index) {
                state.goods.splice(index, 1)
            },
            RESET_GOODS (state) {
                state.goods = []
                state.total_count = 0
                state.current_page = 0
                state.max_page = 0
                state.highlighted = []
            },
            SET_MAX_PAGE (state, max_page) {
                state.max_page = max_page
            },
            SET_CURRENT_PAGE (state, current_page) {
                state.current_page = current_page
            },
            SET_CATEGORIES (state, categories) {
                state.categories = categories
            },
            SET_CURRENCIES (state, currencies) {
                state.currencies = currencies
            },
            SET_CURRENT_GOOD (state, good) {
                state.current_good = good
            }
        },
        actions: {
            async resetGoods({ state, commit, getters, dispatch }) {
                await commit('RESET_GOODS');
            },
            async fetchGoods ({ state, commit, getters, dispatch }, query_params) {
                try {
                    query_params.page = getters['goodsPage'] + 1
                    
                    //total exceed
                    if(query_params.page >= state.max_page && state.max_page > 0) {
                        return
                    }
        
                    var { data } = await axios.get('/api/goods', {
                        params: query_params
                    })
                    
                    var curr_page = state.current_page + 1
                    
                    data = data['data'];

                    commit('SET_MAX_PAGE', data.last_page)
                    commit('SET_CURRENT_PAGE', curr_page)
        
                    var goods = state.goods;
                    data['list'].forEach(function(element) {
                        
                        //if somehow already exists
                        // var exists = views.find(x => (x.id != undefined && x.id === element.id))
                        // if(exists) {
                        //     var exists_index = views.indexOf(exists)
                        //     commit('REMOVE_GOOD', exists_index)
                        // }
                    
                        if(element != undefined) {
                            if (element.highlighted) {
                                commit('ADD_HIGHLIGHTED', element);
                            } else {
                                commit('ADD_GOOD', element);
                            }
                        }
                    });
                } catch (e) {
                    console.log(e)
                    return
                }
            },
            async fetchCurrencies ({ state, commit, getters, dispatch }) {
                try {
                    var { data } = await axios.get('/api/currencies')

                    data = data['data'];
                    commit('SET_CURRENCIES', data);
                } catch (e) {
                    console.log(e)
                    return
                }
            },
            async fetchCategories ({ state, commit, getters, dispatch }) {
                try {
                    var { data } = await axios.get('/api/categories')
                    
                    var curr_page = state.current_page + 1
                    
                    data = data['data'];
                    
                    commit('SET_CATEGORIES', data);
                } catch (e) {
                    console.log(e)
                    return
                }
            },

            async fetchGoodInfo ({ state, commit, getters, dispatch }, slug) {
                try {

                    var { data } = await axios.get('/api/goods/' + slug)

                    data = data['data'];

                    commit('SET_CURRENT_GOOD', data)
                } catch (e) {
                    console.log(e)
                    return
                }
            }
        }
    }
)