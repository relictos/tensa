import axios from 'axios'

// state
export const state = () => ({
    goods: [],
    total_count: 0,
    page_count: 10,
    current_page: 1,
    max_page: 0,
});

// getters
export const getters = {
    goods: function(state) {
        return state.goods
    },
    goodsCount: function(state) {
        return state.goods.length
    },
    goodsPage: function(state) {
        return state.current_page
    },
    goodsExceed: function(state) {
        return state.current_page == state.max_page;
    },
  }

  export const mutations = {
    SET_GOODS (state, goods) {
        state.goods = goods
    },
    ADD_GOOD (state, good) {
        state.good.push(good)
    },
    REMOVE_GOOD (state, index) {
        state.goods.splice(index, 1)
    },
    RESET_GOODS (state) {
        state.goods = []
        state.total_count = 0
        state.current_page = 0
        state.max_page = 0
    },
    SET_MAX_PAGE (state, max_page) {
        state.max_page = max_page
    },
    SET_CURRENT_PAGE (state, current_page) {
        state.current_page = current_page
    }
  }

  export const actions = {
    async fetchGoods ({ state, commit, getters, dispatch }, query_params) {
        try {
            query_params.page = getters['goodsPage'] + 1
            
            //total exceed
            if(query_params.page >= state.max_page && state.max_page > 0) {
                return
            }

            const { data } = await axios.get('/goods', {
                params: query_params
            })

            var curr_page = state.current_page + 1

            commit('SET_MAX_PAGE', data.last_page)
            commit('SET_CURRENT_PAGE', curr_page)

            var goods = state.goods;
            data.list.forEach(function(element) {
                
                //if somehow already exists
                // var exists = views.find(x => (x.id != undefined && x.id === element.id))
                // if(exists) {
                //     var exists_index = views.indexOf(exists)
                //     commit('REMOVE_PLAYER_VIEW', exists_index)
                // }
            
                if(element != undefined)
                    commit('ADD_GOOD', element)
            });

            //console.log(state.player_views)
            //commit('SET_PLAYER_VIEWS', views)

        } catch (e) {
            console.log(e)
            return
        }
    }
  }