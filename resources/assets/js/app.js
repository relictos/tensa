import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

Vue.use(VueRouter);

import App from './views/App'
import Goods from './views/Goods'
import Good from './views/Good'

import Partners from './views/Partners'
import About from './views/About'
import Work from './views/Work'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'goods',
            title: 'Товары',
            component: Goods,
            exact: true,
            meta: {
                showModal: false,
            },
            children: [
                {
                    path: "goods/:goodId",
                    name: "Good page",
                    components: {
                      good: Good
                    },
                    meta: {
                      showModal: true
                    }
                }
            ]
        },
        {
            path: '/work',
            name: 'work',
            title: 'Схема работы',
            component: Work
        },
        {
            path: '/partners',
            name: 'partners',
            title: 'Партнеры',
            component: Partners
        },
        {
            path: '/about',
            name: 'about',
            title: 'О нас',
            component: About
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store
});