<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Производство цветных металлов в Каменске-Уральском</title>
    <meta name="description" content="Tensa - производство цветных металлов в Каменске-Уральском">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" href="/icon.png" type="image/x-icon">
</head>
<body>
<div id="app">
    <app></app>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>