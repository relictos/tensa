let mix = require('laravel-mix');
const resolve = require('path').resolve

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({ extractVueStyles: 'css/app.css' }).options({
    postCss: [
	    require('autoprefixer')()
    ]
});

mix.js('resources/assets/js/app.js', 'public/js')