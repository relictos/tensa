FROM webdevops/php-nginx:alpine

ADD vshost.conf /etc/nginx/conf.d/default.conf
RUN apk add php7-memcached php7-pdo php7-pgsql php7-pdo_pgsql

EXPOSE 80
EXPOSE 443
