<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Currency;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('value')->default('0');
            $table->timestamps();
        });

        Currency::insert([
            'slug' => 'usd',
            'value' => '60'
        ]);
        Currency::insert([
            'slug' => 'euro',
            'value' => '60'
        ]);
        Currency::insert([
            'slug' => 'al',
            'value' => '60'
        ]);
        Currency::insert([
            'slug' => 'cu',
            'value' => '60'
        ]);
        Currency::insert([
            'slug' => 'ni',
            'value' => '60'
        ]);
        Currency::insert([
            'slug' => 'zn',
            'value' => '60'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
