<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->integer('category_id');
            $table->integer('price');
            $table->string('price_suffix');
            $table->string('picture');
            $table->text('description');
            $table->boolean('highlighted')->default(false);
            $table->timestamps();
        });

        Schema::create('good_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('good_id');
            $table->integer('stat_id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->string('name');
            $table->string('contact');
            $table->text('text');
            $table->boolean('watched')->default(false);
            $table->timestamps();
        });

        Schema::create('order_goods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('good_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
        Schema::dropIfExists('good_stats');
        Schema::dropIfExists('stats');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_goods');
    }
}
