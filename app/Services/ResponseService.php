<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Cache;
use Illuminate\Support\Facades\Request as RequestFacade;

class ResponseService
{

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     * @throws \BadMethodCallException
     */
    public static function ok(array $data = [])
    {
        $cacheParams = request()->cache;

        if ($cacheParams && $cacheParams['cached'] && isset($data['data'])) {
            if ('forever' === $cacheParams['cache_time']) {
                Cache::tags($cacheParams['cache_tags'])->forever($cacheParams['cache_key'], $data['data']);
            } else {
                Cache::tags($cacheParams['cache_tags'])
                    ->put($cacheParams['cache_key'], $data['data'], $cacheParams['cache_time']);
            }
        }

        return static::send(
            static::parse($data),
            Response::HTTP_OK,
            $cacheParams['cached']
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     * @throws \BadMethodCallException
     */
    public static function alreadyReported(array $data = [])
    {
        return static::send(static::parse($data), Response::HTTP_ALREADY_REPORTED);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function badRequest(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function unauthorized(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_UNAUTHORIZED
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function forbidden(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_FORBIDDEN
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function notFound(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_NOT_FOUND
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function notAcceptable(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_NOT_ACCEPTABLE
        );
    }

    /**
     * Response for cases when underlying resource is locked by somebody else
     *
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function locked(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_LOCKED
        );
    }

    /**
     * Response for cases when the request can not be executed because of a conflicting request to the resource.
     *
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function conflict(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_CONFLICT
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public static function internalServerError(array $data = [])
    {
        return static::send(
            static::parse($data, false),
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * @param $data
     * @param bool $success
     * @return mixed
     */
    private static function parse($data, $success = true)
    {
        if (!isset($data['data'])) {
            $data['data'] = [];
        }

        if (!isset($data['errors'])) {
            $data['errors'] = [];
        }

        $data['success'] = $success;

        return $data;
    }

    /**
     * @param $data
     * @param int $code
     * @return mixed
     * @throws \InvalidArgumentException
     */
    private static function send($data, $code = Response::HTTP_OK, $cached = false)
    {
        $response = response()->json($data, $code, [
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset'      => 'utf-8'
        ], JSON_UNESCAPED_UNICODE);

        return $response;
    }
}
