<?php

namespace App\Services;

use App\Models\Order;
use App\Exceptions\OrderException;

class OrdersService
{
    const ORDER_MAX_COUNT_PER_IP = 3;

    private $orderModel;
    private $ip = null;
    private $name = null;
    private $contact = null;
    private $text = null;

    public function __construct(Order $orderModel)
    {
        $this->orderModel = $orderModel;
    }

    private function checkIp()
    {
        $ordersCount = $this->orderModel->whereIp($this->ip)->count();
        
        if ($ordersCount > $this::ORDER_MAX_COUNT_PER_IP) {
            throw new OrderException();
        }
    }

    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    public function setName($name) 
    {
        $this->name = $name;

        return $this;
    }

    public function setContact($contact) 
    {
        $this->contact = $contact;

        return $this;
    }

    public function setText($text) 
    {
        $this->text = $text;

        return $this;
    }

    public function createOrder()
    {
        $this->checkIp();

        $order = $this->orderModel->create([
            'name' => $this->name,
            'ip' => $this->ip,
            'contact' => $this->contact,
            'text' => $this->text,
            'watched' => false
        ]);

        return $order;
    }
}
