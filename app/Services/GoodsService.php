<?php

namespace App\Services;

use App\Models\Good;
use App\Models\Category;
use App\Models\Currency;

class GoodsService
{
    private $goodModel;
    private $category = null;
    private $search = null;
    private $goodSlug = null;

    public function __construct(Good $goodModel, Category $categoryModel)
    {
        $this->goodModel = $goodModel;
        $this->categoryModel = $categoryModel;
    }

    public function setCategory($categoryId)
    {
        $this->category = $categoryId;

        return $this;
    }

    public function setSearchQuery($searchQuery)
    {
        $this->search = $searchQuery;

        return $this;
    }

    public function listCurrencies()
    {
        return Currency::all();
    }

    public function listCategories()
    {
        $categories = $this->categoryModel->get();

        return $categories;
    }

    public function listGoods()
    {
        $goods = $this->goodModel->where('id', '>', 0);

        if ($this->category) {
            $goods->whereCategoryId($this->category);
        }

        if ($this->search) {
            $goods->searchByName($this->search);

        }

        return $goods->orderBy('highlighted', 'desc')->orderBy('created_at', 'desc')->paginate();
    }

    public function setGoodSlug($slug)
    {
        $this->goodSlug = $slug;

        return $this;
    }

    public function goodInfo()
    {
        $good = $this->goodModel->with('category', 'stats')->whereSlug($this->goodSlug)->first();

        return $good;
    }
}
