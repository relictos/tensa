<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['ip', 'name', 'contact', 'text', 'watched'];
    
    public function goods()
    {
        return $this->hasMany(OrderGood::class);
    }
}
