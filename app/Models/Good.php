<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function stats()
    {
        return $this->hasMany(GoodStat::class);
    }

    public function scopeSearchByName($query, $search)
    {
        $query->where('name', 'like', '%'.$search)
            ->orWhere('name', 'like', $search.'%')
            ->orWhere('name', 'like', '%'.$search.'%');
    }
}
