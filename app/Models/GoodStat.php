<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodStat extends Model
{
    public function info()
    {
        return $this->belongsTo(Stat::class, 'stat_id', 'id');
    }
}
