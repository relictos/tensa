<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderGood extends Model
{
    public function good()
    {
        return $this->belongsTo(Good::class);
    }
}
