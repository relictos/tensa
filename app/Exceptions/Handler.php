<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        OrderException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJsonResponseForException(Request $request, Exception $e)
    {
        return response()->internalServerError(['errors' => 'order exception']);
        if ($e instanceof OrderException) {
            return response()->internalServerError(['errors' => 'order exception']);
        }

        if ($e instanceof HttpResponseException) {
            return $e->getResponse();
        }

        if ($e instanceof NotFoundHttpException) {
            return response()->notFound(['errors' => 'not found']);
        }

        if ($e instanceof ModelNotFoundException) {
            return response()->notFound(['errors' => 'not found']);
        }

        if ($e instanceof ApplicationException) {
            return response()->internalServerError(['errors' => 'internal server error']);
        }

        if ($e instanceof ForbiddenException) {
            return response()->forbidden([
                'data'   => $e->getData(),
                'errors' => $e->getMessage()
            ]);
        }

        if ($e instanceof AuthorizationException) {
            return response()->forbidden(['errors' => 'unauthorized']);
        }

        if ($e instanceof ValidationException || $e instanceof CustomValidationException) {
            return response()->badRequest(['errors' => $e->errors()]);
        }

        if ($e instanceof OrderException) {
            dd(13);
            return response()->internalServerError(['errors' => 'order exception']);
        }

        if ($e instanceof NotAcceptableHttpException) {
            return response()->notAcceptable(['errors' => $e->getMessage()]);
        }

        return response()->internalServerError(['errors' => 'internal server error']);
    }

    
    /**
     * Determines if request is an api call.
     *
     * If the request URI contains api prefix
     *
     * @param Request $request
     * @return bool
     */
    protected function isApiCall(Request $request)
    {
        return strpos($request->getUri(), env('API_PREFIX', 'api')) !== false;
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (app()->environment('local') || !(strpos($request->getUri(), 'api'))) {
            $response = parent::render($request, $e);
        } else {
            if ($e instanceof OrderException) {
                $response = response()->internalServerError(['errors' => 'order exception']);
            } else {
                $response = response()->internalServerError(['errors' => 'internal server error']);
            }
        }

        return $response;
    }

    
}
