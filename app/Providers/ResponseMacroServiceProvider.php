<?php

namespace App\Providers;

use Response;
use App\Services\ResponseService;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     * @throws \BadMethodCallException
     * @throws \InvalidArgumentException
     */
    public function boot()
    {
        Response::macro('ok', function ($data = []) {
            return ResponseService::ok($data);
        });

        Response::macro('alreadyReported', function ($data = []) {
            return ResponseService::alreadyReported($data);
        });

        Response::macro('badRequest', function ($data = []) {
            return ResponseService::badRequest($data);
        });

        Response::macro('unauthorized', function ($data = []) {
            return ResponseService::unauthorized($data);
        });

        Response::macro('forbidden', function ($data = []) {
            return ResponseService::forbidden($data);
        });

        Response::macro('notFound', function ($data = []) {
            return ResponseService::notFound($data);
        });

        Response::macro('locked', function ($data = []) {
            return ResponseService::locked($data);
        });

        Response::macro('notAcceptable', function ($data = []) {
            return ResponseService::notAcceptable($data);
        });

        Response::macro('conflict', function ($data = []) {
            return ResponseService::conflict($data);
        });

        Response::macro('internalServerError', function ($data = []) {
            return ResponseService::internalServerError($data);
        });
    }
}
