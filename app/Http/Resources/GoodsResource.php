<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GoodsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'slug'         => $this->slug,
            'name'         => $this->name,
            'price'        => $this->price,
            'price_suffix' => $this->price_suffix,
            'picture'      => '/storage/'.str_replace('\\', '/', $this->picture),
            'description'  => $this->description,
            'highlighted'  => $this->highlighted,
            'category'     => CategoryResource::make($this->category),
            'stats'        => StatResource::collection($this->whenLoaded('stats'))
        ];
    }
}
