<?php

namespace App\Http\Controllers;

use App\Http\Requests\GoodsRequest;
use App\Http\Requests\GoodInfoRequest;
use App\Http\Requests\OrderRequest;

use App\Http\Resources\GoodsResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CurrencyResource;

use App\Services\GoodsService;
use App\Services\OrdersService;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    private $service;
    private $orderService;

    public function __construct(GoodsService $service, OrdersService $orderService)
    {
        $this->service = $service;
        $this->orderService = $orderService;
    }

    /*Currencies
    *
    */
    public function currencies()
    {
        $currencies = $this->service->listCurrencies();

        return response()->ok(['data' => CurrencyResource::collection($currencies)]);
    }

    /*Goods list
     *
     */
    public function goods(GoodsRequest $request)
    {
        $goods = $this->service
            ->setCategory($request->category)
            ->setSearchQuery($request->search)
            ->listGoods()
        ;

        return response()->ok(['data' => [
            'list' => GoodsResource::collection($goods),
            'current_page' => $goods->currentPage(),
            'last_page' => $goods->lastPage()
        ]]);
    }

    /**Good page
     *
     */
    public function goodInfo(GoodInfoRequest $request, $slug)
    {
        $good = $this->service
            ->setGoodSlug($slug)
            ->goodInfo();

        return response()->ok(['data' => GoodsResource::make($good)]);
    }

    /**Categories
     * 
     */
    public function categories()
    {
        $categories = $this->service->listCategories();

        return response()->ok(['data' => CategoryResource::collection($categories)]);
    }

    /**Order creation
     *
     */
    public function makeOrder(OrderRequest $request)
    {
        $order = $this->orderService
                    ->setIp(request()->ip())
                    ->setName($request->name)
                    ->setContact($request->contact)
                    ->setText($request->text)
                ->createOrder();

        return response()->ok();
    }
}
